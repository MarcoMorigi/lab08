/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * 
 * Denotes constant related to the place where activities (such as sports) can
 * be practices.
 * 
 */
public enum Place {

    /**
     * Indoor.
     */
    INDOOR,
    /**
     * Outdoor.
     */
    OUTDOOR

}
